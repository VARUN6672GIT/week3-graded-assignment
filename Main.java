package Assignment3;
import java.util.*;
public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s1=new Scanner(System.in);
		MagicOfBooks mb=new MagicOfBooks();
		while (true) {
			System.out.println("\n1. To add a new book\n" + "2. To delete a book\n" + "3. To update a book\n" + "4. To display all the books\n" + "5. To see the total count of the books\n"+ "6. To see the all the books under Autobiography genre\n" +"7. To arrange the book in the following order\n  --price low to high\n  --price high to low\n  --best selling");
			System.out.println("Enter your choice from above list:");
			int choice = s1.nextInt();  
			switch (choice) {
			case 1://Adding a book																			
				System.out.println("No.of Books to add?");
				int n=s1.nextInt();
						
				for(int i=1;i<=n;i++) {
					mb.addbook();
				}
				break;
			case 2://Deleting a book
				mb.deletebook();
				break;
			case 3://Updating a book
				mb.updatebook();
				break;
			case 4:
				mb.displayBookInfo();
				break;
			case 5:
				System.out.println("Count of all books-");
				mb.count();
				break;
			case 6:
				mb.autobiography();
				break;
			case 7:
				System.out.println("1. Price low to high \n2.Price high to low \n3. Best selling \nEnter your choice from above list:\n ");
				int ch = s1.nextInt();
				switch (ch) {
				case 1 : mb.displayByFeature(1);
					break;
				case 2:mb.displayByFeature(2); 
					break;
				case 3:mb.displayByFeature(3);
					break;
				}
			default:
				System.out.println("entered wrong choice.!");
				}
			}
		}
	}
