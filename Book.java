package Assignment3;
public class Book {
	int id;
	String name;
	Double price;
	String Genre;
	int noOfCopiesSold;
	String bookstatus;
	
	public int getId() {
		return id;
		}
	public void setId1(int id) {
		this.id = id;
		}
	public String getName1() {
		return name;
		}
	public void setName1(String name) {
		this.name = name;
		}
	public Double getPrice1() {
		return price;
		}
	public void setPrice(Double price) {
		this.price = price;
		}
	public String getGenre1() {
		return Genre;
		}
	public void setGenre1(String genre) {
		this.Genre = genre;
		}
	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
		}
	public void setNoOfCopiesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
		}
	public String getBookstatus() {
		return bookstatus;
		}
	public void setBookstatus(String bookstatus) {
		this.bookstatus = bookstatus;
		}
	public String toString1() {
		return "Book [id=" + id + ", name=" + name + ", price=" + price + ", genre=" + Genre + ", noOfCopiesSold=" + noOfCopiesSold + ", bookstatus=" + bookstatus + "]";
		}
	public void setId(int id) {
		this.id = id;
		}
	public String getName() {
		return name;
		}
	public void setName(String name) {
		this.name = name;
		}
	public Double getPrice() {
		return price;
		}
	public void setPrice(int price) {
		this.price = (double) price;
		}
	public String getGenre() {
		return Genre;
		}
	public void setGenre(String genre) {
		this.Genre = genre;
		}
	@Override
	public String toString(){
		return "id ->" + id + " name->" + name + " price->" + price + " genre->" + Genre + "\n";
		}
	}
